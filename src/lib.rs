#![cfg_attr(feature = "dox", feature(doc_cfg))]

pub use ffi;
#[doc(hidden)]
pub use glib;

mod monitor_iter;

#[allow(unused_imports)]
#[allow(clippy::let_and_return)]
#[allow(clippy::type_complexity)]
mod auto;

pub use crate::auto::*;
