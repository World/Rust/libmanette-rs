use crate::{Device, MonitorIter};
use glib::translate::*;
use std::ptr;

impl Iterator for MonitorIter {
    type Item = Device;

    #[doc(alias = "manette_monitor_iter_next")]
    fn next(&mut self) -> Option<Self::Item> {
        unsafe {
            let mut device = ptr::null_mut();
            let ret = from_glib(ffi::manette_monitor_iter_next(
                self.to_glib_none_mut().0,
                &mut device,
            ));
            if ret {
                Some(from_glib_none(device))
            } else {
                None
            }
        }
    }
}
